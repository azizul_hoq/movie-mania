# MovieMania

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.3.

## Install packages

Run `npm install` in the root folder

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Production Build

Run `npm run build` to build the project in production mode. The build artifacts will be stored in the `prod/movie-mania` directory.

## Development Build

Run `npm run build-dev` to build the project in development mode. The build artifacts will be stored in the `dev/movie-mania` directory.

## Development Build

Run `npm run build-stage` to build the project in stagging mode. The build artifacts will be stored in the `stage/movie-mania` directory.
