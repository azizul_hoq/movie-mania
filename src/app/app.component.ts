import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import {ThemePalette} from '@angular/material/core';
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  public color: ThemePalette = 'accent';
  public isLoading: boolean;
  public mode: ProgressSpinnerMode = 'indeterminate';
  public value = 50;
  private $unsubscribeAll: Subject<any>;

  constructor(private _router: Router) {
    this.isLoading = false;
    this.$unsubscribeAll = new Subject();
  }

  ngOnInit(): void {
    this._registerRouterEvents();
  }

  private _registerRouterEvents(): void {

    this._router.events
      .pipe(
        takeUntil(this.$unsubscribeAll)
      ).subscribe(event => {
        if (event instanceof NavigationStart) {
          this.isLoading = true;
        } else if (event instanceof NavigationEnd) {
          window.scrollTo(0, 0);
          setTimeout(() => {
            this.isLoading = false;
          }, 1000);
        }
      });

  }

  ngOnDestroy(): void {
    this.$unsubscribeAll.next(null);
    this.$unsubscribeAll.complete();
  }

}
