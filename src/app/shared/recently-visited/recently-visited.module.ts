import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecentlyVisitedComponent } from './recently-visited.component';
import { MovieCardModule } from '../movie-card/movie-card.module';
import { CarouselModule } from 'primeng/carousel';

@NgModule({
  imports: [
    CommonModule,
    MovieCardModule,
    CarouselModule
  ],
  declarations: [RecentlyVisitedComponent],
  exports: [RecentlyVisitedComponent]
})
export class RecentlyVisitedModule { }
