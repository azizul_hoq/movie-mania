import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/core/models/movie.model';
import { WebStorageService } from 'src/app/core/services/web-storage.service';

@Component({
  selector: 'app-recently-visited',
  templateUrl: './recently-visited.component.html',
  styleUrls: ['./recently-visited.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecentlyVisitedComponent implements OnInit {

  public historyList: Movie[];
  public responsiveOptions: any[] = [
    {
      breakpoint: '1024px',
      numVisible: 3,
      numScroll: 3
    },
    {
      breakpoint: '768px',
      numVisible: 2,
      numScroll: 2
    },
    {
      breakpoint: '560px',
      numVisible: 1,
      numScroll: 1
    }
  ];

  constructor(
    private _storageService: WebStorageService
  ) {
    this.historyList = [];
  }

  ngOnInit() {
    this._getRecentlyViewedMovies();
  }

  private _getRecentlyViewedMovies(): void {
    let item: any = this._storageService.getLocalItem(this._storageService.LOCAL_ITEMS.history);
    this.historyList = JSON.parse(item) || [];
  }

  public trackBy(data: any): any {
    return data?.id || null;
  }

}
