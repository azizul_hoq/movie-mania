import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Movie } from 'src/app/core/models/movie.model';
import { WebStorageService } from 'src/app/core/services/web-storage.service';

@Component({
  selector: 'app-watchlist-button',
  templateUrl: './watchlist-button.component.html',
  styleUrls: ['./watchlist-button.component.css']
})
export class WatchlistButtonComponent implements OnInit, OnChanges {

  @Input('movie') movie: Movie = new Movie();
  isWatchlisted: boolean;

  constructor(
    private _webStorage: WebStorageService
  ) {
    this.isWatchlisted = false;
  }

  ngOnChanges(changes: SimpleChanges): void {
    
    if(changes['movie']?.currentValue?.id) {
      let movie: any = this.watchList.find(watch => watch.id === this.movie.id);
      this.isWatchlisted = movie !== undefined;
    }

  }

  ngOnInit() {
  }

  public onClickAddWatchlist(event: Event, movie: Movie) {

    event.stopPropagation();

    let watchList: Movie[] = this.watchList;

    let index = watchList.findIndex(watch => watch.id === movie.id);
    if (index > -1) {
      this.isWatchlisted = false;
      watchList.splice(index, 1);
    } else {
      this.isWatchlisted = true;
      watchList.unshift(movie);
    }

    this._webStorage.setLocalItem(
      this._webStorage.LOCAL_ITEMS.watch_list,
      JSON.stringify(watchList)
    );

  }

  public get watchList(): Movie[] {
    return JSON.parse(this._webStorage.getLocalItem(this._webStorage.LOCAL_ITEMS.watch_list) as any) || [];
  }

}
