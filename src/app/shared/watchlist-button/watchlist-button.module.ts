import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WatchlistButtonComponent } from './watchlist-button.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [WatchlistButtonComponent],
  exports: [WatchlistButtonComponent]
})
export class WatchlistButtonModule { }
