import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieCardComponent } from './movie-card.component';
import { RouterModule } from '@angular/router';
import { WatchlistButtonModule } from '../watchlist-button/watchlist-button.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    WatchlistButtonModule
  ],
  declarations: [MovieCardComponent],
  exports: [MovieCardComponent]
})
export class MovieCardModule { }
