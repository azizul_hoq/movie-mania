import { ChangeDetectionStrategy, Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Movie } from 'src/app/core/models/movie.model';
import { WebStorageService } from 'src/app/core/services/web-storage.service';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieCardComponent implements OnInit {

  @Input('movie') movie: Movie = new Movie();

  constructor() {}

  ngOnInit() {
  }

}
