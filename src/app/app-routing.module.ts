import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [
  {
    path: '', redirectTo: 'movies', pathMatch: 'full'
  },
  {
    path: 'movies',
    loadChildren: () => import('./featured/movies/movies.module').then(m => m.MoviesModule)
  },
  {
    path: 'watchlist',
    loadChildren: () => import('./featured/watchlist/watchlist.module').then(m => m.WatchlistModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
