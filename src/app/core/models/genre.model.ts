interface IGenre {
    id: any;
    name: string;
}

export class Genre implements IGenre {
    
    public id: any;
    public name: string;

    constructor(options: any = {}) {
        this.id = options.id || null;
        this.name = options.name || '';
    }
}