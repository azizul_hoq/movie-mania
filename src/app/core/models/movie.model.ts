import { Genre } from "./genre.model"

interface IMovie {
    adult: boolean;
    backdrop_path: string;
    belongs_to_collection: ICollectionBelongsTo;
    budget: number;
    genres: Genre[];
    homepage: string;
    id: any;
    imdb_id: string;
    original_language: string;
    original_title: string;
    overview: string;
    popularity: number;
    poster_path: string;
    production_companies: IProductionCompany[];
    production_countries: IProductionCountry[];
    release_date: string;
    revenue: number;
    runtime: number;
    spoken_languages: ISpokenLanguage[];
    status: string;
    tagline: string;
    title: string;
    video: boolean;
    vote_average: number;
    vote_count: number;
}

interface ICollectionBelongsTo {
    id: any;
    name: string;
    poster_path: string;
    backdrop_path: string;
}

interface IProductionCompany {
    id: any;
    logo_path: string;
    name: string;
    origin_country: string;
}

interface IProductionCountry {
    iso_3166_1: string;
    name: string;
}

interface ISpokenLanguage {
    english_name: string;
    iso_639_1: string;
    name: string;
}

export class Movie implements IMovie{
    
    public adult: boolean;
    public backdrop_path: string;
    public belongs_to_collection: ICollectionBelongsTo;
    public budget: number;
    public genres: Genre[];
    public homepage: string;
    public id: any;
    public imdb_id: string;
    public original_language: string;
    public original_title: string;
    public overview: string;
    public popularity: number;
    public poster_path: string;
    public production_companies: IProductionCompany[];
    public production_countries: IProductionCountry[];
    public release_date: string;
    public revenue: number;
    public runtime: number;
    public spoken_languages: ISpokenLanguage[];
    public status: string;
    public tagline: string;
    public title: string;
    public video: boolean;
    public vote_average: number;
    public vote_count: number;

    constructor(options: any = {}) {
        this.adult = options.adult || false;
        this.backdrop_path = options.backdrop_path || "";
        this.belongs_to_collection = options.belongs_to_collection || {};
        this.budget = options.budget || 0;
        this.genres = options.genres || [];
        this.homepage = options.homepage || "";
        this.id = options.id || "";
        this.imdb_id = options.imdb_id || "";
        this.original_language = options.original_language || "";
        this.original_title = options.original_title || "";
        this.overview = options.overview || "";
        this.popularity = options.popularity || 0;
        this.poster_path = options.poster_path || "";
        this.production_companies = options.production_companies || [];
        this.production_countries = options.production_countries || [];
        this.release_date = options.release_date || "";
        this.revenue = options.revenue || 0;
        this.runtime = options.runtime || 0;
        this.spoken_languages = options.spoken_languages || [];
        this.status = options.status || "";
        this.tagline = options.tagline || "";
        this.title = options.title || "";
        this.video = options.video || false;
        this.vote_average = options.vote_average || 0;
        this.vote_count = options.vote_count || 0;
    }

}
