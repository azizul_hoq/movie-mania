import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private _apiService: ApiService) { }

  public getAllGenreList(): Observable<any> {
    return this._apiService.get('genre/movie/list');
  }
  
  public getMoviesByGenre(id: string, params?: any): Observable<any> {
    return this._apiService.get(`genre/${id}/movies`, params);
  }

  public discoverMovies(params?: any): Observable<any> {
    return this._apiService.get(`discover/movie`, params);
  }

  public getMovie(id: string): Observable<any> {
    return this._apiService.get(`movie/${id}`);
  }

  public getMovieCredits(id: string): Observable<any> {
    return this._apiService.get(`movie/${id}/credits`);
  }

  public getRelatedMovies(id: string): Observable<any> {
    return this._apiService.get(`movie/${id}/recommendations`);
  }
  
  public getMovieVideos(id: string): Observable<any> {
    return this._apiService.get(`movie/${id}/videos`);
  }
}
