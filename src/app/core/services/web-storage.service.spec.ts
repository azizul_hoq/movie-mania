/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WebStorageService } from './web-storage.service';

describe('Service: WebStorage', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WebStorageService]
    });
  });

  it('should ...', inject([WebStorageService], (service: WebStorageService) => {
    expect(service).toBeTruthy();
  }));
});
