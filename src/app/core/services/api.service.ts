import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private API_URL = environment.api_url;
  private API_KEY = environment.api_key;
  
  constructor(private http: HttpClient) { }

  public get(url: string, paramsData?: any, isApiKey: boolean = true): Observable<any> {
    let params = this._getHttpParams(paramsData, isApiKey);
    return this.http.get(`${this.API_URL}${url}`, { params });
  }

  public post(url: string, data: any, paramsData?: any, isApiKey: boolean = true): Observable<any> {
    let params = this._getHttpParams(paramsData, isApiKey);
    return this.http.post(`${this.API_URL}${url}`, data, { params });
  }

  public put(url: string, data: any, paramsData?: any, isApiKey: boolean = true): Observable<any> {
    let params = this._getHttpParams(paramsData, isApiKey);
    return this.http.put(`${this.API_URL}${url}`, data, { params });
  }

  public patch(url: string, data: any, paramsData?: any, isApiKey: boolean = true): Observable<any> {
    let params = this._getHttpParams(paramsData, isApiKey);
    return this.http.patch(`${this.API_URL}${url}`, data, { params });
  }

  public delete(url: string, paramsData?: any, isApiKey: boolean = true): Observable<any> {
    let params = this._getHttpParams(paramsData, isApiKey);
    return this.http.delete(`${this.API_URL}${url}`, { params });
  }

  private _getHttpParams(paramsData?: any, isApiKey: boolean = true): HttpParams {
    
    let httpParams = new HttpParams();
    if (paramsData !== undefined) {
      Object.getOwnPropertyNames(paramsData).forEach(key => {
        httpParams = httpParams.set(key, paramsData[key]);
      });
    }

    if (isApiKey) {
      httpParams = httpParams.set('api_key', this.API_KEY);
    }

    return httpParams;
  }

}
