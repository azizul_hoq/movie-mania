import { Injectable } from '@angular/core';
import { LOCAL_ITEMS, SESSION_ITEMS } from '../constants/movie.constant';

@Injectable({
  providedIn: 'root'
})
export class WebStorageService {

  public readonly LOCAL_ITEMS = LOCAL_ITEMS;
  public readonly SESSION_ITEMS = SESSION_ITEMS;

  constructor() { }

  getLocalItem(key: string): string | null {
    return localStorage.getItem(key);
  }
  
  setLocalItem(key: string, value: string): void {
    localStorage.setItem(key,value);
  }
  
  removeLocalItem(key: string): void {
    localStorage.removeItem(key);
  }
  
  clearLocalStorage(): void {
    localStorage.clear();
  }
  
  getSessionItem(key: string): string | null {
    return sessionStorage.getItem(key);
  }
  
  setSessionItem(key: string, value: string): void {
    sessionStorage.setItem(key,value);
  }
  
  removeSessionItem(key: string): void {
    sessionStorage.removeItem(key);
  }
  
  clearSessionStorage(): void {
    sessionStorage.clear();
  }
}
