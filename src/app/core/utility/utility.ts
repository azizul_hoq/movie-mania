export function getRandomList(list: any[], size: number): any[] {
    let result = new Array(size),
        length = list.length,
        taken = new Array(length);
    if (size > length)
        return list;
    while (size--) {
        let random = Math.floor(Math.random() * length);
        result[size] = list[random in taken ? taken[random] : random];
        taken[random] = --length in taken ? taken[length] : length;
    }
    return result;
}