interface IMenu {
    name: string,
    path: string;
    icon?: string;
    children?: IMenu;
}

export const MENUS: IMenu[] = [
    {
        name: 'Movies',
        path: 'movies'
    },
    {
        name: 'Watchlist',
        path: 'watchlist'
    }
] 