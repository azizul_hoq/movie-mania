import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderNavComponent } from './header-nav/header-nav.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    HeaderNavComponent,
    FooterComponent
  ],
  exports: [
    HeaderNavComponent,
    FooterComponent
  ]
})
export class StructureModule { }
