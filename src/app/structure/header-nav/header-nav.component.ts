import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MENUS } from 'src/app/core/constants/menu.constant';

@Component({
  selector: 'app-header-nav',
  templateUrl: './header-nav.component.html',
  styleUrls: ['./header-nav.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderNavComponent implements OnInit {

  public readonly menuList = MENUS;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public onClickMenu(url: string): void {
    this.router.navigate([url])
  }

}
