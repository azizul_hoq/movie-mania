import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { catchError, Observable, of, Subject, switchMap, takeUntil } from 'rxjs';
import { Movie } from 'src/app/core/models/movie.model';
import { MovieService } from 'src/app/core/services/movie.service';
import { GenreDataService } from '../services/genre-data.service';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.css']
})
export class GenresComponent implements OnInit, OnDestroy {

  private $_unsubscribeAll: Subject<any>;
  public genreId: string;
  public genreName: string;
  private _topLength: number;
  private _movieSortBy: string;
  public isLoading: boolean;
  public totalResults: number;
  public page: number;

  constructor(
    public dataService: GenreDataService,
    private _movieService: MovieService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) {

    this.$_unsubscribeAll = new Subject();
    this.genreId = "";
    this.genreName = "";
    this._topLength = 10;
    this._movieSortBy = 'vote_average.desc';
    this.isLoading = false;
    this.totalResults = 0;
    this.page = 1;
  }

  ngOnInit() {
    this._getParamsData();
  }

  private _getParamsData(): void {

    this.isLoading = true;
    
    this._activatedRoute.params
      .pipe(
        takeUntil(this.$_unsubscribeAll),

        switchMap((params: Params) => {

          this.genreId = params['id'];
          this.genreName = params['name'];

          if (!this.genreId) return this._router.navigate(['/']);

          return this._discoverTopMoviesByGenre();
        }),

        catchError(error => {
          return this._router.navigate(['/']);
        })

      ).subscribe(response => {
        this.isLoading = false;
        this.dataService.topMoviesList = (response.results || []).slice(0, this._topLength);
        this._getMovieListByGenre(this.genreId);
      });
  }

  private _discoverTopMoviesByGenre(): Observable<any> {
    let ulrParams = {
      sort_by: this._movieSortBy,
      with_genres: this.genreId
    }
    return this._movieService.discoverMovies(ulrParams);
  }

  private _getMovieListByGenre(genreId: string, page: number = 1): void{
    
    this.isLoading = true;

    let params = { page };
    this._movieService.getMoviesByGenre(genreId, params)
      .pipe(
        takeUntil(this.$_unsubscribeAll),
        catchError(error => {
          this.isLoading = false;
          return of({})
        })
      ).subscribe(response => {
        this.isLoading = false;
        this.dataService.moviesList = (response.results || []);
        this.totalResults = response.total_results || 0;
      });
  }

  public changePage(event: PageEvent): void {
    this.page = event.pageIndex + 1;
    this._getMovieListByGenre(this.genreId, this.page);
  }

  public get topMoviesList(): Movie[] {
      return this.dataService.topMoviesList;
  }
  
  public get moviesList(): Movie[] {
      return this.dataService.moviesList;
  }

  public trackBy(data: any): any {
    return data?.id || null;
  }

  ngOnDestroy(): void {
    this.$_unsubscribeAll.next(null);
    this.$_unsubscribeAll.complete();
  }

}
