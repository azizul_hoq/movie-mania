import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenresComponent } from './components/genres.component';
import { RouterModule, Routes } from '@angular/router';
import { GenreDataService } from './services/genre-data.service';
import { MovieCardModule } from 'src/app/shared/movie-card/movie-card.module';
import {MatPaginatorModule} from '@angular/material/paginator';

const ROUTES: Routes = [
  {
    path: ':id/:name', component: GenresComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    MovieCardModule,
    MatPaginatorModule
  ],
  declarations: [GenresComponent],
  providers: [GenreDataService]
})
export class GenresModule { }
