import { Injectable } from '@angular/core';
import { Movie } from 'src/app/core/models/movie.model';

@Injectable()
export class GenreDataService {

  public topMoviesList: Movie[];
  public moviesList: Movie[];

  constructor() { 
    this.topMoviesList = [];
    this.moviesList = [];
  }

}
