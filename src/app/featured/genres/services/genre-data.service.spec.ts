/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GenreDataService } from './genre-data.service';

describe('Service: GenreData', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenreDataService]
    });
  });

  it('should ...', inject([GenreDataService], (service: GenreDataService) => {
    expect(service).toBeTruthy();
  }));
});
