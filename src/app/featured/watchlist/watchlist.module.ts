import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WatchlistComponent } from './components/watchlist.component';
import { RouterModule, Routes } from '@angular/router';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MovieCardModule } from 'src/app/shared/movie-card/movie-card.module';
import { WatchlistDataService } from './services/watchlist-data.service';

const ROUTES: Routes = [
  {
    path: '', component: WatchlistComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    MovieCardModule,
    MatPaginatorModule
  ],
  declarations: [WatchlistComponent],
  providers: [WatchlistDataService]
})
export class WatchlistModule { }
