import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Movie } from 'src/app/core/models/movie.model';
import { WatchlistResponse } from '../models/watchlis-response.model';
import { WatchlistDataService } from '../services/watchlist-data.service';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {

  public totalResults: number;
  public itemsPerPage: number;
  public pageIndex: number;

  constructor(
    public dataService: WatchlistDataService
  ) {
    this.totalResults = 0;
    this.pageIndex = 1;
    this.itemsPerPage = 20;
  }

  ngOnInit() {
    this._getWatchList();
  }

  private _getWatchList(page: number = 1, items: number = 20): void {
    let response: WatchlistResponse = this.dataService.getWatchlist(page, items);
    this.totalResults = response.totalResults;
    this.dataService.watchList = response.results;
  }

  public changePage(event: PageEvent): void {
    this.pageIndex = event.pageIndex + 1;
    this._getWatchList(this.pageIndex, this.itemsPerPage);
  }

  public get watchList(): Movie[] {
    return this.dataService.watchList;
  }

  public trackBy(data: any): any {
    return data?.id || null;
  }

}
