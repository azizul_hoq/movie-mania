/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WatchlistDataService } from './watchlist-data.service';

describe('Service: WatchlistData', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WatchlistDataService]
    });
  });

  it('should ...', inject([WatchlistDataService], (service: WatchlistDataService) => {
    expect(service).toBeTruthy();
  }));
});
