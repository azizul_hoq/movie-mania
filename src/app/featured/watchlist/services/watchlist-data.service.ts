import { Injectable } from '@angular/core';
import { Movie } from 'src/app/core/models/movie.model';
import { WebStorageService } from 'src/app/core/services/web-storage.service';
import { WatchlistResponse } from '../models/watchlis-response.model';

@Injectable()
export class WatchlistDataService {

  public watchList: Movie[];

  constructor(
    private _storageService: WebStorageService
  ) {
    this.watchList = [];
  }

  public getWatchlist(page: number = 1, items: number = 20): WatchlistResponse {
    let index = items * (page - 1);
    let allWatchlist: Movie[] = this._allWatchlist;
    let results: Movie[] = allWatchlist.slice(index, items * page);
    return new WatchlistResponse({ results, totalResults: allWatchlist.length });
  }

  private get _allWatchlist(): Movie[] {
    let item: any = this._storageService.getLocalItem(this._storageService.LOCAL_ITEMS.watch_list);
    return JSON.parse(item) || [];
  }

}
