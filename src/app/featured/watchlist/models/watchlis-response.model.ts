import { Movie } from "src/app/core/models/movie.model";

interface IWatchlistResponse {
    results: Movie[];
    totalResults: number;
}

export class WatchlistResponse implements IWatchlistResponse {
    results: Movie[];
    totalResults: number;

    constructor(options: any = {}) {
        this.results = options.results || [];
        this.totalResults = options.totalResults || 0;
    }
}