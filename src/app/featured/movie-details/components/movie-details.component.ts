import { Component, OnDestroy, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { catchError, of, Subject, takeUntil } from 'rxjs';
import { Movie } from 'src/app/core/models/movie.model';
import { MovieService } from 'src/app/core/services/movie.service';
import { Video } from '../models/video.model';
import { MovieDetalsDataService } from '../services/movie-detals-data.service';
import { MatDialog } from '@angular/material/dialog';
import { TrailerVideoComponent } from './modals/trailer-video/trailer-video.component';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit, OnDestroy {

  private _unsubscribeAll: Subject<any>;
  public movieId: string;
  private _youtubeBaseUrl: string;
  private _autoplay: string;

  constructor(
    public dataService: MovieDetalsDataService,
    private _activatedRoute: ActivatedRoute,
    private _movieService: MovieService,
    private _sanitizer: DomSanitizer,
    private _dialog: MatDialog
  ) {
    this._unsubscribeAll = new Subject();
    this.movieId = "";
    this._youtubeBaseUrl = "https://www.youtube.com/embed/";
    this._autoplay = "?rel=0;&autoplay=1&mute=0";
  }

  ngOnInit() {
    this._getInitialData();
  }

  private _getInitialData(): void {

    this._activatedRoute.params
      .pipe(
        takeUntil(this._unsubscribeAll)
      ).subscribe((params: Params) => {
        this.movieId = params['id'];
        this._getMovieDetailsBy(this.movieId);
        this._getCastAndCrewList(this.movieId);
        this._getRelatedMovies(this.movieId);
        this._getMovieVideos(this.movieId);
      });
  }

  private _getMovieDetailsBy(movieId: string): void {
    this._movieService.getMovie(movieId)
      .pipe(
        takeUntil(this._unsubscribeAll)
      ).subscribe((movie: Movie) => {
        this.dataService.movie = movie;
        this._setHistory(movie);
      });
  }

  private _setHistory(movie: Movie): void {
    this.dataService.setHistory(movie);
  }

  private _getCastAndCrewList(movieId: string): void {

    this._movieService.getMovieCredits(movieId)
      .pipe(
        takeUntil(this._unsubscribeAll),
        catchError(() => of({}))
      ).subscribe(response => {
        this.dataService.castList = response.cast || [];
        this.dataService.crewList = response.crew || [];
      });

  }

  private _getRelatedMovies(movieId: string): void {

    this._movieService.getRelatedMovies(movieId)
      .pipe(
        takeUntil(this._unsubscribeAll),
        catchError(() => of({}))
      ).subscribe(response => {
        this.dataService.relatedMovieList = response.results || [];
      });

  }

  private _getMovieVideos(movieId: string): void {
    this._movieService.getMovieVideos(movieId)
      .pipe(
        takeUntil(this._unsubscribeAll),
        catchError(() => of({}))
      ).subscribe(response => {
        this.dataService.trailerVideo = this.dataService.getTrailerVideo(response.results);
      });
  }

  public onClickWatchTrailer(video: Video) {
    this.trailerVideo.url = this._sanitizer.bypassSecurityTrustResourceUrl(this._youtubeBaseUrl + video.key + this._autoplay); 
    this._dialog.open(TrailerVideoComponent, {
      height: '560px',
      width: '900px',
      data: { video: this.trailerVideo }
    });
  }

  public get movie(): Movie {
    return this.dataService.movie;
  }
  
  public get trailerVideo(): Video {
    return this.dataService.trailerVideo;
  }

  public get castList(): any [] {
    return this.dataService.castList;
  }
  
  public get crewList(): any [] {
    return this.dataService.crewList;
  }
  
  public get relatedMovieList(): any [] {
    return this.dataService.relatedMovieList;
  }

  public trackBy(data: any): any {
    return data?.id || null;
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

}
