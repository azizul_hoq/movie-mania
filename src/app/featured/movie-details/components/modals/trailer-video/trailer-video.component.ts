import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-trailer-video',
  templateUrl: './trailer-video.component.html',
  styleUrls: ['./trailer-video.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrailerVideoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<TrailerVideoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

}
