import { Injectable } from '@angular/core';
import { Movie } from 'src/app/core/models/movie.model';
import { WebStorageService } from 'src/app/core/services/web-storage.service';
import { Cast } from '../models/cast.model';
import { Crew } from '../models/crew.model';
import { Video } from '../models/video.model';

@Injectable()
export class MovieDetalsDataService {

  public movie: Movie;
  public castList: Cast[];
  public crewList: Crew[];
  public relatedMovieList: Movie[];
  public trailerVideo: Video;

  public responsiveOptions: any[] = [
    {
      breakpoint: '1024px',
      numVisible: 3,
      numScroll: 3
    },
    {
      breakpoint: '768px',
      numVisible: 2,
      numScroll: 2
    },
    {
      breakpoint: '560px',
      numVisible: 1,
      numScroll: 1
    }
  ];

  constructor(
    private _storageService: WebStorageService
  ) {
    this.movie = new Movie();
    this.trailerVideo = new Video();
    this.castList = [];
    this.crewList = [];
    this.relatedMovieList = [];
  }

  public getTrailerVideo(videoList: Video[] = []): Video {

    if (videoList?.length) {
          
      let trailers: Video[] = videoList.filter((video: Video) => video.type === 'Trailer');
      if (trailers.length) {
        let officialTrailer = trailers.find((video: Video) => video.name === 'Official Trailer');
        return officialTrailer || trailers[0];
      } else {
        return videoList[0];
      }
      
    }

    return new Video(); 
  }

  public setHistory(movie: Movie): void {

    let item: any = this._storageService.getLocalItem(this._storageService.LOCAL_ITEMS.history);
    let historyList: Movie[] = JSON.parse(item) || [];
    let index = historyList.findIndex(history => history.id == movie.id);
    if (index > -1) {
      historyList.splice(index, 1);
    }

    historyList.unshift(movie);

    if (historyList.length > 20) historyList.length = 20;

    item = JSON.stringify(historyList);
    this._storageService.setLocalItem(this._storageService.LOCAL_ITEMS.history, item);

  }

}
