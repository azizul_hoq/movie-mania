interface ICast {
    adult: boolean;
    cast_id: number;
    character: string;
    credit_id: string
    gender: number;
    id: number;
    known_for_department: string;
    name: string;
    order: number;
    original_name: string;
    popularity: number;
    profile_path: string;
}

export class Cast implements ICast {
    adult: boolean;
    cast_id: number;
    character: string;
    credit_id: string
    gender: number;
    id: number;
    known_for_department: string;
    name: string;
    order: number;
    original_name: string;
    popularity: number;
    profile_path: string;

    constructor(options: any = {}) {
        this.adult = options.adult || false;
        this.cast_id = options.cast_id || null;
        this.character = options.character || "";
        this.credit_id = options.credit_id || "";
        this.gender = options.gender || 0;
        this.id = options.id || null;
        this.known_for_department = options.known_for_department || "";
        this.name = options.name || "";
        this.order = options.order || null;
        this.original_name = options.original_name || "";
        this.popularity = options.popularity || null;
        this.profile_path = options.profile_path || "";
    }
}