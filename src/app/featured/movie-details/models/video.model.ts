import { SafeResourceUrl } from "@angular/platform-browser";

interface IVideo {
    iso_639_1: string;
    iso_3166_1: string;
    name: string;
    key: string;
    site: string;
    size: number;
    type: string;
    official: boolean;
    published_at: string;
    id: string;
}

export class Video implements IVideo {
    iso_639_1: string;
    iso_3166_1: string;
    name: string;
    key: string;
    site: string;
    size: number;
    type: string;
    official: boolean;
    published_at: string;
    id: string;
    url: string | SafeResourceUrl;

    constructor(options: any = {}) {
        this.iso_639_1 = options.iso_639_1 || "";
        this.iso_3166_1 = options.iso_3166_1 || "";
        this.name = options.name || "";
        this.key = options.key || "";
        this.site = options.site || "";
        this.size = options.size || 0;
        this.type = options.type || "";
        this.official = options.official || "";
        this.published_at = options.published_at || "";
        this.id = options.id || "";
        this.url = options.url || "";
    }
}