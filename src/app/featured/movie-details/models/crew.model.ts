interface ICrew {
    adult: boolean;
    credit_id: string;
    department: string;
    gender: number;
    id: number;
    job: string;
    known_for_department: string;
    name: string;
    original_name: string;
    popularity: number;
    profile_path: string;
}

export class Crew implements ICrew {

    adult: boolean;
    credit_id: string;
    department: string;
    gender: number;
    id: number;
    job: string;
    known_for_department: string;
    name: string;
    original_name: string;
    popularity: number;
    profile_path: string;

    constructor(options: any = {}) {
        this.adult = options.adult || false;
        this.credit_id = options.credit_id || "";
        this.department = options.department || "";
        this.gender = options.gender || 0;
        this.id = options.id || null;
        this.job = options.job || "";
        this.known_for_department = options.known_for_department || "";
        this.name = options.name || "";
        this.original_name = options.original_name || "";
        this.popularity = options.popularity || null;
        this.profile_path = options.profile_path || "";
    }

}