import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieDetailsComponent } from './components/movie-details.component';
import { MovieDetalsDataService } from './services/movie-detals-data.service';
import { RouterModule, Routes } from '@angular/router';
import { CarouselModule } from 'primeng/carousel';
import { MovieCardModule } from 'src/app/shared/movie-card/movie-card.module';
import { TrailerVideoComponent } from './components/modals/trailer-video/trailer-video.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { WatchlistButtonModule } from 'src/app/shared/watchlist-button/watchlist-button.module';

const ROUTES: Routes = [
  {
    path: '', component: MovieDetailsComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    CarouselModule,
    MovieCardModule,
    WatchlistButtonModule,
    MatDialogModule,
    MatIconModule
  ],
  declarations: [
    MovieDetailsComponent,
    TrailerVideoComponent
  ],
  providers: [MovieDetalsDataService]
})
export class MovieDetailsModule { }
