import { Genre } from "src/app/core/models/genre.model";
import { Movie } from "src/app/core/models/movie.model";

interface IGenreMovie extends Genre {
    movies: Movie[];
}

export class GenreMovie implements IGenreMovie {
    public id: any;
    public name: string;
    public movies: Movie[];

    constructor(options: any = {}) {
        this.id = options.id || 0;
        this.name = options.name || '';
        this.movies = options.movies || [];
    }
}