import { Component, OnDestroy, OnInit } from '@angular/core';
import { catchError, of, Subject, takeUntil } from 'rxjs';
import { Genre } from 'src/app/core/models/genre.model';
import { MovieService } from 'src/app/core/services/movie.service';
import { GenreMovie } from '../models/genre-movie.model';
import { MovieDataService } from '../services/movie-data.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit, OnDestroy {

  public throttle = 300;
  public scrollDistance = 1;
  public scrollUpDistance = 2;
  private _unsubscribeAll: Subject<any>;
  private _listCounter: number = 0;

  constructor(
    private _movieService: MovieService,
    public dataService: MovieDataService
  ) {
    this._unsubscribeAll = new Subject();
  }

  ngOnInit() {
    this._getAllGenreList();
  }

  private _getAllGenreList(): void {

    this._movieService.getAllGenreList()
      .pipe(
        takeUntil(this._unsubscribeAll),
        catchError((error) => {
          console.error(error);
          return of({});
        })
      ).subscribe(response => {
        this.dataService.genreList = response.genres || [];
        // initially get first 3(maximum) genres movie list
        this._generateMoviesChunkByGenre(this.dataService.genreList);
      });

  }

  private _generateMoviesChunkByGenre(genreList: Genre[]): void {
    // max list size will be 3
    let list: Genre[] = genreList.slice(this._listCounter, this._listCounter + 3);
    if (list.length == 0) return;
    this._listCounter += 3;
    this._getMoviesByGenre(list);
  }

  private _getMoviesByGenre(genreList: Genre[]): void {

    for (let index = 0; index < genreList.length; index++) {
      this._movieService.getMoviesByGenre(genreList[index].id)
      .pipe(
        takeUntil(this._unsubscribeAll)
      ).subscribe(response => {

        let genreMovie: GenreMovie = this.dataService.prepareGenreMovieData(response.results, genreList[index]);
        this.genreMovieList.push(genreMovie);

      });
    }

  }

  public onScrollDown(): void {
    this._generateMoviesChunkByGenre(this.dataService.genreList);
  }

  public get genreMovieList(): GenreMovie[] {
    return this.dataService.genreMovieList;
  }

  trackBy(data: any) {
    return data?.id || null;
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(null);
    this._unsubscribeAll.complete();
  }

}
