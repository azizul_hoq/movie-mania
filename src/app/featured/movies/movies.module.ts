import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesComponent } from './components/movies.component';
import { RouterModule, Routes } from '@angular/router';
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { MovieDataService } from './services/movie-data.service';
import { MovieCardModule } from 'src/app/shared/movie-card/movie-card.module';
import { RecentlyVisitedModule } from 'src/app/shared/recently-visited/recently-visited.module';

const ROUTES: Routes = [
  {
    path: '', component: MoviesComponent
  },
  {
    path: 'genre',
    loadChildren: () => import('../genres/genres.module').then(m => m.GenresModule)
  },
  {
    path: ':id',
    loadChildren: () => import('../movie-details/movie-details.module').then(m => m.MovieDetailsModule)
  }
];

@NgModule({
  imports: [
    CommonModule,
    MovieCardModule,
    InfiniteScrollModule,
    RouterModule.forChild(ROUTES),
    RecentlyVisitedModule
  ],
  declarations: [MoviesComponent],
  providers: [MovieDataService]
})
export class MoviesModule { }
