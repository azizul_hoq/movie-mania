import { Injectable } from '@angular/core';
import { Genre } from 'src/app/core/models/genre.model';
import { Movie } from 'src/app/core/models/movie.model';
import { getRandomList } from 'src/app/core/utility/utility';
import { GenreMovie } from '../models/genre-movie.model';

@Injectable()
export class MovieDataService {

  public genreList: Genre[];
  public genreMovieList: GenreMovie[];

  constructor() {
    this.genreList = [];
    this.genreMovieList = [];
  }

  public prepareGenreMovieData(movieList: Movie[], genre: Genre): GenreMovie {
    let genreMovie: GenreMovie = new GenreMovie();
    genreMovie.movies = getRandomList(movieList, 5);
    Object.assign(genreMovie, genre);
    return genreMovie;
  }

}
