/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MovieDataService } from './movie-data.service';

describe('Service: MovieData', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MovieDataService]
    });
  });

  it('should ...', inject([MovieDataService], (service: MovieDataService) => {
    expect(service).toBeTruthy();
  }));
});
