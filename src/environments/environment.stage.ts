import { CONFIG } from "./config/config";

export const environment = {
    production: false,
    api_key: CONFIG.stage.api_key,
    api_url: `${CONFIG.stage.protocol}${CONFIG.stage.prefix}${CONFIG.stage.base_url}${CONFIG.stage.postfix}`
};