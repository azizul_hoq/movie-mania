import { CONFIG } from "./config/config";

export const environment = {
    production: false,
    api_key: CONFIG.dev.api_key,
    api_url: `${CONFIG.dev.protocol}${CONFIG.dev.prefix}${CONFIG.dev.base_url}${CONFIG.dev.postfix}`
};