import { CONFIG } from "./config/config";

export const environment = {
  production: true,
  api_key: CONFIG.prod.api_key,
  api_url: `${CONFIG.prod.protocol}${CONFIG.prod.prefix}${CONFIG.prod.base_url}${CONFIG.prod.postfix}`
};
